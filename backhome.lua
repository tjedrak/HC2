--[[
%% properties
%% globals
inHome
isArmed
--]]

local message1 = "It is after sunset so I turned your lights on. I also turned on your devices and disarmed home. Have a great evening!";
local message2 = "I turned on your devices and disarmed home alarm. Have a nice day!";

local sourceTrigger = fibaro:getSourceTrigger();
if (sourceTrigger["type"] == "other") then
  if fibaro:getGlobalValue("inHome") == "No" then
		fibaro:startScene(106); -- Disarm home
    	fibaro:debug("Disarmed.");
		fibaro:startScene(114); -- Devices ON
    	fibaro:debug("Devices ON.");
    	if fibaro:getGlobalValue("isNight") == "Yes"
 		then
 			fibaro:startScene(122) -- Światła
			fibaro:debug("Night back. Lights On.")
      		fibaro:call(2, "sendEmail", "Fibaro Home System", message1);
		else
      		fibaro:call(2, "sendEmail", "Fibaro Home System", message2);
      	end
		fibaro:setGlobal("inHome", "Yes");
	  	fibaro:debug("Back home.");
    else
    	fibaro:debug("You are already at Home.")
    end
end
fibaro:debug("Scene ends.")
fibaro:abort()