--[[
%% autostart
%% properties
%% globals
--]]

local luxBathSensor = 88
local motionBathSensor = 86
local checkLux = 3
local inBath
luxValue = tonumber(fibaro:getValue(luxBathSensor, "value"))

if (tonumber(fibaro:countScenes()) > 1) then 
 	fibaro:debug("Killing another scene.") 
 	fibaro:abort()
end

function CheckMotionBath()
 		if tonumber(fibaro:getValue(motionBathSensor, "value")) == 1	
    	then
    		fibaro:debug("Someone in Bath. Light: "..luxValue.." lux.")
      		inBath = 1
      	else
    		fibaro:debug("No one in Bath. Light: "..luxValue.." lux.")
      		inBath = 0
      	end
    	fibaro:sleep(1000)
end

while true do
  		CheckMotionBath()
  		if inBath == 0 and tonumber(fibaro:getValue(luxBathSensor, "value")) > 10 then	
    			fibaro:debug("Posible light in Bathroom.")
				fibaro:debug(checkLux.." attempts for checking.")
    			if inBath == 0 and checkLux == 0 then
					fibaro:call(2, "sendEmail", "Fibaro Home System", "Probably you have left lights turned on in the bathroom!");
      				fibaro:sleep(30*60*1000) -- Wait 30 min
      				checkLux = 3
				end
   				fibaro:debug("Lights ON. Checking if someone is in Bath.")
    			checkLux = checkLux - 1 -- Checking left
    			if checkLux == 1 and inBath == 1 then
					checkLux = 3
					fibaro:debug("Noticed someone in Bath. Reseting checks.")
				end
  		end
  		fibaro:sleep(15*60*1000) -- Wait 5 min
  end
