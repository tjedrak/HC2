--[[
%% autostart
%% properties 
24 value 
%% globals 
--]]

local camera = 110 --id Camera 
local czujkaId = 24

fibaro:debug("[ALARM] Scene activated.")

if (tonumber(fibaro:countScenes()) > 1) then 
 	fibaro:debug("Killing another scene.") 
 	fibaro:abort()
end

if fibaro:getGlobalValue("Breached") == "No" and fibaro:getGlobalValue("Alarm") == "Disarmed" 
  then 
	fibaro:debug("Alarm: "..fibaro:getGlobalValue("Alarm")..". Breached: "..fibaro:getGlobalValue("Breached")..". Move sensor: ("..tonumber(fibaro:getValue(czujkaId, "value")).."). Scene ends.") 
	fibaro:abort() 
end 

while true do
  if fibaro:getGlobalValue("Alarm") == "Armed" and fibaro:getGlobalValue("Breached") == "Yes" then
  	fibaro:call(2, "sendEmail", "Fibaro Home System", "Someone is breaching your home. Sending you screens from camera.");
    if tonumber(fibaro:getValue(czujkaId, "value")) == 1
    then
    	fibaro:call(126, "turnOn") -- Light office
    	fibaro:call(125, "turnOn") -- Light living room
  		fibaro:call(camera, "sendPhotoToEmail", "tjedrak@gmail.com")
		fibaro:debug("[BREACHED]. Motion detected. Sending photo.")
		fibaro:sleep(1000)
    end
  	if tonumber(fibaro:getValue(czujkaId, "value")) == 0 and fibaro:getGlobalValue("isNight") == "No"
     then
    	fibaro:call(126, "turnOff") -- Light office
    	fibaro:call(125, "turnOff") -- Light living room
    	fibaro:debug("[BREACHED] No move. Turning lights off.")
        fibaro:sleep(1000)
	end
    if tonumber(fibaro:getValue(czujkaId, "value")) == 0 and fibaro:getGlobalValue("isNight") == "Yes"
     then
    	fibaro:debug("[BREACHED] No move. Waiting.")
    	fibaro:sleep(2*1000)
	end
  end
  fibaro:sleep(1000)
end

fibaro:debug("Scene ends.")