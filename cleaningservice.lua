--[[
%% autostarts
%% properties
%% globals
CleaningHome
--]]



local checkDay = os.date("*t");
local dni = {"niedziela", "poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota"}
local allSensors = {86, 24, 154} -- Motion Sensore
local Cleaner = 0 -- Clean Service IN[1]/OUT[0]
local Checks = 3 -- How many checks for Cleaner
dayOfCleaning = tonumber(fibaro:getGlobalValue("dayOfCleaning"))

if (tonumber(fibaro:countScenes()) > 1) then 
 	fibaro:debug("Killing another scene.") 
 	fibaro:abort()
end

function CheckCleanService()
 		if tonumber(fibaro:getValue(allSensors[1], "value")) == 1
    	or tonumber(fibaro:getValue(allSensors[2], "value")) == 1
    	or tonumber(fibaro:getValue(allSensors[3], "value")) == 1	
    	then
    		fibaro:debug("Home BREACHED. Probably Clean services is IN.")
      		Cleaner = 1
      	else
    		fibaro:debug("Home SAFE. Probably Clean services is OUT.")
      		Cleaner = 0
      	end
    	fibaro:sleep(1000)
end

while true do
  	
  	if checkDay.wday ~= dayOfCleaning then 
		fibaro:debug("It's "..dni[checkDay.wday]..".")
    	fibaro:sleep(60*60*1000) -- Wait 5 minutes
	end
	if fibaro:getGlobalValue("inHome") == "Yes" then
		fibaro:debug("I'm in Home now.")
    	fibaro:sleep(60*60*1000) -- Wait 5 minutes
    end
    
    if checkDay.wday == dayOfCleaning then 
    
	CheckCleanService() -- Checking clean service
  
		if fibaro:getGlobalValue("CleaningService") == "Searching" and Cleaner == 1 then -- Searching for cleaning service
				fibaro:setGlobal("CleaningService", "Probably");
				fibaro:debug("Cleaning service probably in HOME. Sending notification.")
				fibaro:startScene(114) -- Start devices
				fibaro:call(2, "sendEmail", "Fibaro Home System", "I've noticed that someone came to your home. Please check if it's a Cleaning Service.");  -- Send to user to decide.
		end
		
		if fibaro:getGlobalValue("CleaningService") == "Probably" then
				fibaro:debug("Wating for your response.")
      			fibaro:call(camera, "sendPhotoToEmail", "tjedrak@gmail.com")
				fibaro:sleep(15*60*1000) -- Wait 15 min for reaction
		end 

		if fibaro:getGlobalValue("CleaningService") == "No" then -- Clean services is OUT
				fibaro:setGlobal("inHome", "Yes"); -- HACK to Arm home
				fibaro:startScene(110) -- Devices off
				fibaro:startScene(121) -- Lights off
				fibaro:startScene(107) -- Arm
				fibaro:call(2, "sendEmail", "Fibaro Home System", "Your Cleaning Service has left your home. I turned the lights and devices off. Also I armed your home.");  -- Send to user to decide. -- Send info to user.
				fibaro:debug("Clean service is OUT. Home armed. Scene ends.")
				fibaro:abort();
		end
	
		if fibaro:getGlobalValue("CleaningService") == "Yes" then
				fibaro:sleep(10*60*1000) -- Wait 5 minute
				if Cleaner == 1 then
					Checks = 3
					fibaro:debug("Noticed Cleaning Service. Reseting checks.")
				end
				fibaro:debug("Waitinig 15 minute to check again.")
				fibaro:debug("There are "..Checks.." attempts.")
				Checks = Checks - 1 -- Checking left
				if Cleaner == 0 and Checks == 0 then
					fibaro:setGlobal("CleaningService", "No");
				end
		end
  	end
end
 
