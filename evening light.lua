--[[ 
%% autostart
%% properties 
154 value 
%% globals 
--]] 

local motionEveningId = 154 --id Motion Sensora 
local lightEveningId = 127 --id elementu wykonawczego 

local opoznienie = 300 --opoznienie wylaczenia swiatla po ustaniu ruchu, w sekundach 
local hourStart = '1600' --godzina, od ktorej scena bedzie wykonywana, format HHMM, typ: string 
local hourEnd = '2359' --godzina, do ktorej scena bedzie wykonywana, format HHMM, typ: string 

fibaro:debug("Scene activated.") 

if fibaro:countScenes() > 1 or fibaro:getGlobalValue("onVacation") == "Yes" or fibaro:getGlobalValue("Sleeping") == "Yes" or fibaro:getGlobalValue("isNight") == "No" or fibaro:getGlobalValue("inHome") == "No" then 
  fibaro:debug("Vacation: "..fibaro:getGlobalValue("onVacation")..".\nSleeping: "..fibaro:getGlobalValue("Sleeping")..".\nNight: "..fibaro:getGlobalValue("isNight")..".\n Or scene duplicated. Scene ends.") 
  fibaro:abort() 
end 

if tonumber(fibaro:getValue(motionEveningId, "value")) == 0 then
  fibaro:debug("Motion not detected!")
  fibaro:abort()
end

if (tonumber(os.date("%H%M")) >= tonumber(hourStart) and tonumber(os.date("%H%M")) <= tonumber(hourEnd)) then 
  fibaro:debug("Hour accepted.") 
else 
  fibaro:debug("Hour not acceptable. Scene ends.") 
  fibaro:abort()
end 


local czas = os.time() 

fibaro:call(lightEveningId, "turnOn") 
fibaro:debug("Lights turn On. Waiting "..opoznienie.." seconds.") 

while true do 
        if os.time() - czas > opoznienie then 
          fibaro:call(lightEveningId, "turnOff"); 
          fibaro:debug("Light turn Off. Scene ends.") 
          fibaro:abort() 
          break 
        end 

        if tonumber(fibaro:getValue(motionEveningId, "value")) > 0 then 
            fibaro:debug("Motion detected. Reseting time.") 
            czas = os.time() 
        end 
	fibaro:sleep(500) 
    
end
