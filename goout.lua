--[[
%% properties
%% globals
inHome
dayOfCleaning
--]]

local message1 = "I turned all devices and lights off. Your home is going into Armed Mode in 5 minutes.";
local message2 = "Your home is now armed and safe. Have a nice day!";

local sourceTrigger = fibaro:getSourceTrigger();
if (sourceTrigger["type"] == "other") then
  if fibaro:getGlobalValue("inHome") == "Yes" then
		fibaro:setGlobal("inHome", "No");
		fibaro:startScene(110); -- Devices OFF
		fibaro:startScene(121); -- Lights OFF
    	fibaro:call(2, "sendEmail", "Fibaro home system", message1);
      	fibaro:debug("Leaved home.");
    	fibaro:sleep(5*60*1000) -- 5 minutes wait
      	fibaro:startScene(107); -- Armed
    	fibaro:call(2, "sendEmail", "Fibaro home system", message2)
    else
    	fibaro:debug("You are already outside.")
    end
    if fibaro:getGlobalValue("Sleeping") == "Yes" then
    	fibaro:debug("You are sleeping.")
    end
end
fibaro:debug("Scene ends.")
fibaro:abort()