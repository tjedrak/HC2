--[[
%% properties
%% globals
enableOn
isSleep
inHome
--]]
local message = "I turned lights and devices off. I also armed the door. Sleep well!"
local sourceTrigger = fibaro:getSourceTrigger();
if (sourceTrigger["type"] == "other") then
	if fibaro:getGlobalValue("Sleeping") == "No" and fibaro:getGlobalValue("inHome") == "Yes" and fibaro:getGlobalValue("onVacation") == "No" 
	then
		fibaro:call(23, "setArmed", "1"); -- Only doors
		fibaro:startScene(110); -- Devices off
		fibaro:startScene(121); -- Lights off
		fibaro:call(2, "sendEmail", "Fibaro home system", message);
		fibaro:setGlobal("inHome", "Yes");
	  	fibaro:setGlobal("Sleeping", "Yes");
		fibaro:setGlobal("Alarm", "Armed");
		fibaro:debug("Sweat dreams!");
    end
    if fibaro:getGlobalValue("Sleeping") == "Yes" then
    	fibaro:debug("You are already sleeping.")
    end
  	if fibaro:getGlobalValue("onVacation") == "Yes" then
    	fibaro:debug("You are on vacations.")
    end
  	if fibaro:getGlobalValue("inHome") == "No" then
    	fibaro:debug("You are not in your Home.")
    end
  
end
fibaro:debug("Scene ends.")
fibaro:abort()

