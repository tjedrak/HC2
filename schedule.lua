--[[ 
%% properties 
%% globals 
%% autostart 
--]]

if (tonumber(fibaro:countScenes()) > 1) then
 fibaro:debug("Killing another scene.") 
 fibaro:abort() -- Maybe kill first scene (and timers) instead and let new scene run? 
end 
local currentDate = os.date("*t");

function sunrise() return hour(fibaro:getValue(1, "sunriseHour"))() end
function sunset() return hour(fibaro:getValue(1, "sunsetHour"))() end
function offset(time, sec)  return function() return time()+sec; end end 
function hour(time)
  return function()
    local t = os.date("*t");
    t.hour,t.min = time:match("(%d+):(%d+)"); t.sec = 0; -- Set hour/min/sec
    return os.time(t);
  end;
end

function schedule(time, msg, action, interval, catchup)
  interval = interval or 60*60*24; -- default interval is 24h
  local diff = time()-os.time(); -- seconds in the future  
  if (diff <= 0) then -- If time has already passed 
    if (catchup == nil) then -- First time around we default to run events in the past 
      action();
      fibaro:debug("Catchup: "..msg.." at "..os.date("%c",os.time()+diff));
    end -- then bump time (default 24h) 
    diff = diff+interval*(1+math.floor(-diff/interval)); -- new time in the future
  end 
  -- run event in the future and reschedule 
  setTimeout(function() fibaro:debug("Calling: "..msg); action(); schedule(time,msg,action,interval,false) end, diff*1000);  
  fibaro:debug("Scheduled next "..msg.." at "..os.date("%c",os.time()+diff)); 
end 

-- Wake up 
if fibaro:getGlobalValue("Weekend") == "No" and fibaro:getGlobalValue("onVacation") == "No" and fibaro:getGlobalValue("inHome") == "Yes" then  
  schedule(hour("05:40"), "[Week] [Scene] Devices ON", function() fibaro:startScene(114) end);
  schedule(hour("05:40"), "[Week] [Scene] Disarm home", function() fibaro:startScene(106) end); 
  schedule(hour("05:40"), "[Week] [Scene] Lights ON", function() fibaro:setGlobal("Lights", "On") end); 
  schedule(hour("05:40"), "[Week] [Email] Goodmorning", function() fibaro:call(2, "sendEmail", "Fibaro Home System", "Good morning sleepy! I turned on your devices and light! I also disarmed a door alarm. Have a great day!"); end); 
  schedule(hour("05:40"), "[Week] [Variable] Sleeping: No", function() fibaro:setGlobal("Sleeping", "No") end);
end

-- Go work (not Monday)
if fibaro:getGlobalValue("Weekend") == "No" and fibaro:getGlobalValue("onVacation") == "No" and currentDate.wday ~= 2 and fibaro:getGlobalValue("inHome") == "Yes" then -- Not Monday (not cleaning)	
  schedule(hour("08:00"), "[Week] [Scene] Lights OFF", function() fibaro:startScene(121) end); -- Lights OFF
  schedule(hour("08:00"), "[Week] [Scene] Devices OFF", function() fibaro:startScene(110) end); -- Devices OFF
  schedule(hour("08:00"), "[Week] [Scene] Arm home", function() fibaro:startScene(107) end); -- Arm home
  schedule(hour("08:00"), "[Week] [Email] Goodbye", function() fibaro:call(2, "sendEmail", "Fibaro Home System", "I turned all devices and lights off. Your home is in Armed Mode. Have a nice day!"); end);
  schedule(hour("08:00"), "[Week] [Variable] inHome: No", function() fibaro:setGlobal("inHome", "No") end);
  
end

-- Go work (Monday)
if fibaro:getGlobalValue("Weekend") == "No" and fibaro:getGlobalValue("onVacation") == "No" and currentDate.wday == 2 and fibaro:getGlobalValue("inHome") == "Yes" then
  schedule(hour("08:00"), "[Monday] [Scene] Lights OFF", function() fibaro:startScene(121) end); 
  schedule(hour("08:00"), "[Monday] [Scene] Devices OFF", function() fibaro:startScene(110) end);
  schedule(hour("08:00"), "[Monday] [Variable] Cleaning Service: searching", function() fibaro:setGlobal("CleaningService", "Searching") end);
  schedule(hour("08:00"), "[Monday] [Variable] inHome: No", function() fibaro:setGlobal("inHome", "No") end);
  schedule(hour("08:00"), "[Monday] [Email] Goodbye", function() fibaro:call(2, "sendEmail", "Fibaro Home System", "I turned all devices and lights off. Now I waiting for the Cleaning Service. Have a great day!"); end);
end

-- Wake Up Weekend
if fibaro:getGlobalValue("Weekend") == "Yes" and fibaro:getGlobalValue("onVacation") == "No" and fibaro:getGlobalValue("inHome") == "Yes" then
  schedule(hour("09:00"), "[Weekend] [Scene] Devices ON", function() fibaro:startScene(114) end);
  schedule(hour("09:00"), "[Weekend] [Scene] Disarm home", function() fibaro:startScene(106) end);
  schedule(hour("09:00"), "[Weekend] [Email] Goodmorning", function() fibaro:call(2, "sendEmail", "Fibaro Home System", "Good morning sleepy. I turned devices on and disarmed a door. Have a nice day!"); end);
end

-- Aroma time
if fibaro:getGlobalValue("Weekend") == "No" and fibaro:getGlobalValue("onVacation") == "No" then
  schedule(hour("15:30"), "[Week] [Device] Aroma ON", function() fibaro:call(29, "turnOn") end);
  schedule(hour("15:30"), "[Week] [Email] Aroma", function() fibaro:call(2, "sendDefinedEmailNotification", "117"); end);
end
