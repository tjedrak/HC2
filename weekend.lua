--[[
%% autostart
%% properties
%% globals
--]]

local currentDate = os.date("*t");

if (tonumber(fibaro:countScenes()) > 1) then 
 	fibaro:debug("Killing another scene.") 
 	fibaro:abort()
end

while true do
    if currentDate.wday == 1 or currentDate.wday == 7 then
        fibaro:setGlobal("Weekend", "Yes");
    	fibaro:debug("It's weekend!")
    else
        fibaro:setGlobal("Weekend", "No");
    	fibaro:debug("It's week.")
    end
  	fibaro:debug("Waiting.")
    fibaro:sleep(60*60*1000);
end
